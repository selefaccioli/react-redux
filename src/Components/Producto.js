import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import { eliminarProductoAction, obtenerProductoEditar } from '../actions/productoAction';

const Producto = ({producto}) => {
    const dispatch = useDispatch();
    const history = useHistory();

    const {nombre, precio, id} = producto;

    const eliminarProducto = id => dispatch( eliminarProductoAction(id) );

    // Funcion que redirige de forma programada
    const redireccionarEdicion = producto => {  
        history.push(`/productos/editar/${producto.id}`);
        dispatch( obtenerProductoEditar(producto) );
    }

    return ( 
        <tr>
            <td>{nombre}</td>
            <td><span className="font-weight-bold"> $ {precio}</span></td>
            <td className="acciones">
                <button type="button" to={`/productos/editar/${id}`} className="btn btn-primary mr-2" onClick={() => redireccionarEdicion(producto)}>
                    Editar
                </button>
                <button
                    type="button"
                    className="btn btn-danger"
                    onClick={() => eliminarProducto(id)}
                >
                    Eliminar
                </button>
            </td>
        </tr>
     );
}
 
export default Producto;