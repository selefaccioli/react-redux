/* eslint-disable import/no-anonymous-default-export */
import {
    AGREGAR_PRODUCTO,
    AGREGAR_PRODUCTO_EXITO,
    AGREGAR_PRODUCTO_ERROR,
    OBTENER_PRODUCTOS,
    OBTENER_PRODUCTOS_EXITO,
    OBTENER_PRODUCTOS_ERROR,
    ELIMINAR_PRODUCTO,
    ELIMINAR_PRODUCTO_CANCELADO,
    ELIMINAR_PRODUCTO_EXITO,
    ELIMINAR_PRODUCTO_ERROR,
    OBTENER_PRODUCTO_EDITAR,
    PRODUCTO_EDITADO_EXITO,
    PRODUCTO_EDITADO_ERROR
} from '../types';

// cada reducrer tiene su propio state
const initialState = {
    productos: [],
    error: null,
    loading: false,
    productoEliminar: null,
    productoEditar: null
}

export default function(state = initialState, action) {
    switch (action.type) {
        case ELIMINAR_PRODUCTO:
        case OBTENER_PRODUCTOS:
        case AGREGAR_PRODUCTO:
            return {
                ...state,
                loading: true,
                error: false
            }

        case AGREGAR_PRODUCTO_EXITO:
            return {
                ...state,
                loading: false,
                productos: [...state.productos, action.payload]
            }    

        case ELIMINAR_PRODUCTO_ERROR:    
        case OBTENER_PRODUCTOS_ERROR:
        case AGREGAR_PRODUCTO_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payload
            }    
            
        case OBTENER_PRODUCTOS_EXITO:
            return {
                ...state,
                loading: false,
                productos: action.payload
            }    
        
        case ELIMINAR_PRODUCTO_EXITO:
            return {
                ...state,
                loading: false,
                productos: state.productos.filter( producto => producto.id !== action.payload )
            }    

        case ELIMINAR_PRODUCTO_CANCELADO:
            return {
                ...state,
                loading: false
            }  
            
        case OBTENER_PRODUCTO_EDITAR:
            return {
                ...state,
                error: false,
                productoEditar: action.payload
            }    

        case PRODUCTO_EDITADO_EXITO:
            return {
                ...state,
                productoEditar: null,
                productos: state.productos.map( producto => producto.id === action.payload.id ? producto = action.payload : producto)
            }    

        case PRODUCTO_EDITADO_ERROR:
            return {
                ...state,
                productoEditar: null,
                error: action.payload
            }    

        default:
            return state;
    }
}