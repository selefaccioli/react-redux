import {
    AGREGAR_PRODUCTO,
    AGREGAR_PRODUCTO_EXITO,
    AGREGAR_PRODUCTO_ERROR,
    OBTENER_PRODUCTOS,
    OBTENER_PRODUCTOS_EXITO,
    OBTENER_PRODUCTOS_ERROR,
    ELIMINAR_PRODUCTO,
    ELIMINAR_PRODUCTO_CANCELADO,
    ELIMINAR_PRODUCTO_EXITO,
    ELIMINAR_PRODUCTO_ERROR,
    OBTENER_PRODUCTO_EDITAR,
    COMENZAR_EDICION_PRODUCTO,
    PRODUCTO_EDITADO_EXITO,
    PRODUCTO_EDITADO_ERROR
} from '../types';

import clienteAxios from '../config/axios';
import Swal from 'sweetalert2';

export function crearNuevoProductoAction(producto) {
    return async (dispatch) => {
        dispatch( agregarProducto() );

        try {
            //Insertar en la API
            await clienteAxios.post('/productos', producto);

            // Si todo sale bien, actualiza el state
            dispatch( agregarProductoExito(producto) );

            Swal.fire(
                'Correcto',
                'El producto se agrego correctamente',
                'success'
            )
        } catch (error) {
            console.log(error);

            //Si hay un error cambiar el state
            dispatch( agregarProductoError(true) );

            Swal.fire({
                icon: 'error',
                title: 'error',
                text: 'Hubo un error, intenta de nuevo'
            })
        }
    }
}

export function obtenerProductosAction() {
    return async (dispatch) => {
        dispatch( obtenerProductos() );

        try {
            const respuesta = await clienteAxios.get('/productos');

            dispatch ( obtenerProductosExito(respuesta.data) );

        } catch (error) {
            console.log(error);

            dispatch( obtenerProductosError(true) );

            Swal.fire({
                icon: 'error',
                title: 'error',
                text: 'Hubo un error, intenta de nuevo'
            })
        }
    }
}

export function eliminarProductoAction(id) {
    return async (dispatch) => {
        dispatch( eliminarProducto() );

        try {
            const producto = await clienteAxios.get(`/productos/${id}`);
            
            if(producto.data) {
                Swal.fire({
                    title: `Esta seguro que desea eliminar ${producto.data.nombre}?`,
                    text: "No podrá revertirse!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, borralo!',
                    cancelButtonText: 'Cancelar'
                  }).then((result) => {
                    if (result.isConfirmed) {

                        clienteAxios.delete(`/productos/${id}`);
                        dispatch( eliminarProductoExito(id) );

                        Swal.fire(
                            'Eliminado!',
                            'Tu Producto ha sido eliminado.',
                            'success'
                        )
                    }

                    if(result.isDismissed) {
                        dispatch( eliminarProductoCancelado() );
                    }
                  })

            }
        } catch (error) {
            console.log(error);
            dispatch( eliminarProductoError(true) );
        }
    }
}

// Colocar producto en edicion 

export function obtenerProductoEditar(producto) {
    return (dispatch) => {
        dispatch( obtenerProductoAction(producto) )
    }
}


// Edita un registro en la api y state
export function editarProductoAction(producto) {
    return async (dispatch) => {
        dispatch( editarProducto() );

        try {
            clienteAxios.put(`/productos/${producto.id}`, producto);
            dispatch( editarProductoExito(producto) );
        } catch (error) {
            console.log(error);
            dispatch( editarProductoError() );
        }
    }
}

const agregarProducto = () => ({
    type: AGREGAR_PRODUCTO
});

// Si el producto se guarda ne la base de datos
const agregarProductoExito = producto => ({
    type: AGREGAR_PRODUCTO_EXITO,
    payload: producto
});

// Si hubo un error
const agregarProductoError = estado => ({
    type: AGREGAR_PRODUCTO_ERROR,
    payload: estado
});

const obtenerProductos = () => ({
    type: OBTENER_PRODUCTOS
});

const obtenerProductosExito = productos => ({
    type: OBTENER_PRODUCTOS_EXITO,
    payload: productos
});

const obtenerProductosError = estado => ({
    type: OBTENER_PRODUCTOS_ERROR,
    payload: estado
});

const eliminarProducto = () => ({
    type: ELIMINAR_PRODUCTO
});

const eliminarProductoExito = id => ({
    type: ELIMINAR_PRODUCTO_EXITO,
    payload: id
});

const eliminarProductoError = estado => ({
    type: ELIMINAR_PRODUCTO_ERROR,
    payload: estado
});

const eliminarProductoCancelado = () => ({
    type: ELIMINAR_PRODUCTO_CANCELADO
});

const obtenerProductoAction = producto => ({
    type: OBTENER_PRODUCTO_EDITAR,
    payload: producto
});

const editarProducto = () => ({
    type: COMENZAR_EDICION_PRODUCTO
});

const editarProductoExito = producto => ({
    type: PRODUCTO_EDITADO_EXITO,
    payload: producto
});

const editarProductoError = () => ({
    type: PRODUCTO_EDITADO_ERROR
});